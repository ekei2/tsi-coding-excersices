function reverseArrayInPlace(array) {
   for (var i = 0; i < Math.floor(array.length / 2); i++) {
      var old = array[i];
      array[i] = array[array.length - 1 - i];
      array[array.length - 1 - i] = old;
   }
   console.log(array);
   return array;
}
var value = [1, 2, 3, 4, 5];
reverseArrayInPlace(value);

// Without a loop
function reverseArray(array) {
    var revers = array.reverse()
    console.log(revers)
    return array;
 }
 var value = [1, 2, 3, 4, 5];
reverseArray(value);
